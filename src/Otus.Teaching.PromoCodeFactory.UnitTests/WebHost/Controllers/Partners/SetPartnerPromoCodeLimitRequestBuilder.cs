﻿using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    internal class SetPartnerPromoCodeLimitRequestBuilder
    {
        private int _limit;
        private DateTime _endTime;

        public SetPartnerPromoCodeLimitRequestBuilder WithEndData(DateTime endTime)
        {
            _endTime = endTime;
            return this;
        }
        
        public SetPartnerPromoCodeLimitRequestBuilder WithLimit(int limit)
        {
            _limit = limit;
            return this;
        }

        public SetPartnerPromoCodeLimitRequest Build()
        {
            return new SetPartnerPromoCodeLimitRequest
            {
                EndDate = _endTime,
                Limit = _limit
            };
        }
    }
}
