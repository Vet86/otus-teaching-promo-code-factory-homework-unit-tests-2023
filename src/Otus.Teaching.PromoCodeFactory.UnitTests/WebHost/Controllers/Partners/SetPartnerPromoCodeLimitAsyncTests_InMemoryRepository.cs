﻿using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;
using Microsoft.Extensions.DependencyInjection;
using FluentAssertions;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests_InMemoryRepository : IClassFixture<TestFixture_InMemoryRepository>
    {
        private readonly IRepository<Partner> _partnersRepository;
        private readonly PartnersController _partnersController;

        public SetPartnerPromoCodeLimitAsyncTests_InMemoryRepository(TestFixture_InMemoryRepository testFixture)
        {
            _partnersRepository = testFixture.ServiceProvider.GetService<IRepository<Partner>>();
            _partnersController = new PartnersController(_partnersRepository);
        }

        [Fact(DisplayName = "Нужно убедиться, что сохранили новый лимит в базу данных с использованием InMemoryRepository")]
        public async Task SetPartnerPromoCodeLimitAsyncWithInMemoryRepository_PositiveLimit_AddedSuccesfully()
        {
            // Arrange
            await InitDataAsync();

            var partnerId = Guid.Parse("0da65561-cf56-4942-bff2-22f50cf70d43");

            var request = new SetPartnerPromoCodeLimitRequestBuilder()
                .WithLimit(5)
                .WithEndData(DateTime.Now)
                .Build();

            // Act
            await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

            // Assert
            var resPartner = (await _partnersRepository.GetByIdAsync(partnerId));
            resPartner.PartnerLimits.Count.Should().Be(2);
            resPartner.PartnerLimits.Last().Limit.Should().Be(5);
        }

        private async Task InitDataAsync()
        {
            var partnerId = Guid.Parse("0da65561-cf56-4942-bff2-22f50cf70d43");

            var partner = new PartnerBuilder()
                .WithId(partnerId)
                .WithActivity(true)
                .WithNumberIssuedPromoCodes(5)
                .WithPartnerPromoCodeLimit(new PartnerPromoCodeLimit())
                .Build();

            await _partnersRepository.AddAsync(partner);
        }
    }
}
