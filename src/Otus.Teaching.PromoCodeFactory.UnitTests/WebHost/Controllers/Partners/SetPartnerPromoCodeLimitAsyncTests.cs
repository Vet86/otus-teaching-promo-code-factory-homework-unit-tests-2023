﻿using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests
    {
        private readonly Mock<IRepository<Partner>> _partnersRepositoryMock;
        private readonly PartnersController _partnersController;

        public SetPartnerPromoCodeLimitAsyncTests()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            _partnersRepositoryMock = fixture.Freeze<Mock<IRepository<Partner>>>();
            _partnersController = fixture.Build<PartnersController>().OmitAutoProperties().Create();
        }

        [Fact(DisplayName = "Если партнер не найден, то также нужно выдать ошибку 404")]
        public async Task SetPartnerPromoCodeLimitAsync_PartnerIsNotFound_ReturnsNotFound()
        {
            // Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync((Partner)null);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, It.IsAny<SetPartnerPromoCodeLimitRequest>());

            // Assert
            result.Should().BeAssignableTo<NotFoundResult>();
        }

        [Fact(DisplayName = "Если партнер заблокирован, то есть поле IsActive=false в классе Partner, то также нужно выдать ошибку 400")]
        public async Task SetPartnerPromoCodeLimitAsync_PartnerIsInactive_ReturnsBadRequest()
        {
            // Arrange
            var partnerId = Guid.Parse("0da65561-cf56-4942-bff2-22f50cf70d43");

            var partner = new PartnerBuilder()
                .WithId(partnerId)
                .WithActivity(false)
                .Build();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, It.IsAny<SetPartnerPromoCodeLimitRequest>());

            // Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }

        [Fact(DisplayName = "Если партнеру выставляется лимит, то мы должны обнулить количество промокодов, которые партнер выдал NumberIssuedPromoCodes")]
        public async Task SetPartnerPromoCodeLimitAsync_SetNewLimit_ResetNumberIssuedPromoCodes()
        {
            // Arrange
            var partnerId = Guid.Parse("0da65561-cf56-4942-bff2-22f50cf70d43");
            
            var partner = new PartnerBuilder()
                .WithId(partnerId)
                .WithActivity(true)
                .WithNumberIssuedPromoCodes(5)
                .WithPartnerPromoCodeLimit(new PartnerPromoCodeLimit())
                .Build();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            var request = new SetPartnerPromoCodeLimitRequestBuilder()
                .WithLimit(2)
                .WithEndData(DateTime.Now)
                .Build();

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

            // Assert
            partner.NumberIssuedPromoCodes.Should().Be(0);
        }

        [Fact(DisplayName = "Если партнеру выставляется лимит и если лимит закончился, то количество промокодов, которые партнер выдал NumberIssuedPromoCodes не обнуляется")]
        public async Task SetPartnerPromoCodeLimitAsync_PromoCodeIfFinished_NumberIssuedPromoCodesNotChanges()
        {
            // Arrange
            var partnerId = Guid.Parse("0da65561-cf56-4942-bff2-22f50cf70d43");

            var partner = new PartnerBuilder()
                .WithId(partnerId)
                .WithActivity(true)
                .WithNumberIssuedPromoCodes(5)
                .WithPartnerPromoCodeLimit(new PartnerPromoCodeLimit() { CancelDate = DateTime.Now} )
                .Build();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            var request = new SetPartnerPromoCodeLimitRequestBuilder()
                .WithLimit(2)
                .WithEndData(DateTime.Now)
                .Build();

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

            // Assert
            partner.NumberIssuedPromoCodes.Should().Be(5);
        }

        [Fact(DisplayName = "При установке лимита нужно отключить предыдущий лимит")]
        public async Task SetPartnerPromoCodeLimitAsync_ResetPreviousLimit_CancelDateNotNull()
        {
            // Arrange
            var partnerId = Guid.Parse("0da65561-cf56-4942-bff2-22f50cf70d43");

            var partnerLimit = new PartnerPromoCodeLimit();

            var partner = new PartnerBuilder()
                .WithId(partnerId)
                .WithActivity(true)
                .WithNumberIssuedPromoCodes(5)
                .WithPartnerPromoCodeLimit(partnerLimit)
                .Build();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            var request = new SetPartnerPromoCodeLimitRequestBuilder()
                .WithLimit(2)
                .WithEndData(DateTime.Now)
                .Build();

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

            // Assert
            partnerLimit.CancelDate.Should().NotBeNull();
        }

        [Theory(DisplayName = "Лимит должен быть больше 0. Положительный сценарий")]
        [InlineData(1)]
        [InlineData(5)]
        [InlineData(10000)]
        public async Task SetPartnerPromoCodeLimitAsync_PositiveLimit_ReturnCreatedAtActionResult(int limit)
        {
            // Arrange
            var partnerId = Guid.Parse("0da65561-cf56-4942-bff2-22f50cf70d43");

            var partner = new PartnerBuilder()
                .WithId(partnerId)
                .WithActivity(true)
                .WithNumberIssuedPromoCodes(5)
                .WithPartnerPromoCodeLimit(new PartnerPromoCodeLimit())
                .Build();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            var request = new SetPartnerPromoCodeLimitRequestBuilder()
                .WithLimit(limit)
                .WithEndData(DateTime.Now)
                .Build();

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

            // Assert
            result.Should().BeAssignableTo<CreatedAtActionResult>();
        }

        [Theory(DisplayName = "Лимит должен быть больше 0. Негативный сценарий")]
        [InlineData(0)]
        [InlineData(-1)]
        [InlineData(-5)]
        [InlineData(-10000)]
        public async Task SetPartnerPromoCodeLimitAsync_NeagativeLimit_ReturnBadRequestObjectResult(int limit)
        {
            // Arrange
            var partnerId = Guid.Parse("0da65561-cf56-4942-bff2-22f50cf70d43");

            var partner = new PartnerBuilder()
                .WithId(partnerId)
                .WithActivity(true)
                .WithNumberIssuedPromoCodes(5)
                .WithPartnerPromoCodeLimit(new PartnerPromoCodeLimit())
                .Build();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            var request = new SetPartnerPromoCodeLimitRequestBuilder()
                .WithLimit(limit)
                .WithEndData(DateTime.Now)
                .Build();

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, request);

            // Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }
    }
}