﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    internal class PartnerBuilder
    {
        private Guid _id;
        private bool _isAcitve;
        private int _numberIssuedPromoCodes;
        private List<PartnerPromoCodeLimit> _partnerPromoCodeLimits = new();

        public PartnerBuilder WithId(Guid id)
        {
            _id = id;
            return this;
        }

        public PartnerBuilder WithActivity(bool isActive)
        {
            _isAcitve = isActive;
            return this;
        }

        public PartnerBuilder WithNumberIssuedPromoCodes(int numberIssuedPromoCodes)
        {
            _numberIssuedPromoCodes = numberIssuedPromoCodes;
            return this;
        }

        public PartnerBuilder WithPartnerPromoCodeLimit(PartnerPromoCodeLimit partnerPromoCodeLimit)
        {
            _partnerPromoCodeLimits.Add(partnerPromoCodeLimit);
            return this;
        }

        public Partner Build()
        {
            return new Partner
            {
                Id = _id,
                IsActive = _isAcitve,
                NumberIssuedPromoCodes = _numberIssuedPromoCodes,
                PartnerLimits = _partnerPromoCodeLimits
            };
        }
    }
}
